Мониторинг системы
==================

Используемые классы
- `\common\components\MonitoringDb`
- `\common\components\OnlineManager`

Используемый кеш используется в Memcache который прописан в `/common/config/main.php` в компоненте `cache`.

Считаются все запросы `REQUEST` на приложение из `avatar-bank`.

- `php yii statistic/calculate` - вычисление раз в мин
- `php yii statistic/clear` - очистка