Построение бизнес модели
========================

За ввод денег из внешней системы отвечают классы
`\common\models\BillingMain` - главный для всех видов валют, имеет сквозную нумерацию
`\common\models\piramida\Billing` - Счет для рублей, которые ведутся в таблицах `nw_*`
`\common\models\MerchantRequest` - Заказ от мерчанта, который имеет свой `\common\models\piramida\Billing`, будет `\common\models\BillingMain`
