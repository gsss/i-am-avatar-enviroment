Процедура очистки банка
=======================

Запускается из консоли командой
yii clear/run

Удаляются 
- все счета `user_bill`
- адреса `user_bill_address`
- операции `user_bill_operation`
- транзакции `user_bill_transaction`
