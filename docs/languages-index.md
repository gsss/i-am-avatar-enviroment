Использование языков
====================

Описывает систему использования нескольких языков, редактирование строковых переменных,
процедуру определения языка пользователя и переключения языка.

# Терминология

- *Идентификатор Языка* - наш идентификатор таблицы `languages.id`.
- *Код Языка* - переменная которая хранится в `Yii::$app->language` по стардарту [ISO 639-1](https://ru.wikipedia.org/wiki/ISO_639).
- *Язык по умолчанию* - *код языка*, который используется если не определен язык по интеллектуальному алгоритму и
тот, который используется в поле основной таблицы при переводе списков (см. Переводы списков)
и в основном поле при переводе полей (см. Переводы полей) `Yii::$app->sourceLanguage`

# Глобальная идеология Интернализации (i18n) и Локализации

Для указания *Кода языка* мы используем двухбуквенный код по стардарту [ISO 639-1](https://ru.wikipedia.org/wiki/ISO_639).
В приложении если необходимо в коде установить глабальный язык приложения это делается следующей командой:
```php
 // ...
 Yii::$app->language = 'ru';
 // ...
```

Устанавливается язык в конфигурации: файл `main.php`
```php
return [
    // Язык "используемый в системе" на которой надодится интерфейс пользователя с которым общается наша система
    'language' => 'ru'

    // Язык "используемый по умолчанию" если не нашлось перевода на язык "используемый в системе"
    'sourceLanguage' => 'ru'
];
```

Стратегия сооставления пользовательских предпочтений и использыемых в системе хранится в конфигурационном файле `main.php` в компоненте `languageService`
```php
return [
    'components' => [
        // ...
        'languageManager'   => [
            'class'    => 'common\components\LanguageManager',
            'strategy' => [
                 'ru' => ['ru', 'be', 'ky', 'ab', 'mo', 'et', 'lv', 'uk'],
                 'en' => ['en', 'de', 'da'],
            ],
        ],
        // ...
    ]
    // ...
];
```

Список используемых языков хранится в таблице `languages`.

### Таблица `languages`

- id - int - autoincrement
- name - string - Название языка
- code - varchar(16) - *Код Языка*, который используется в переменной `Yii::$app->language`
...

Которая содержит еще и таблицу с переводами языков `languages_translation` (см. Перевод списков)

# Описание

Что переводим:
 - строки на странице
 - картинки на странцие
 - ошибочные сообщения в коде
 - списки, например список городов, список языков, список групп пользователей

Для того чтобы язык приложения всегда был установлен верно, то есть чтобы язык пользователя
был установлен соответствующий язык приложения, то есть `Yii::$app->language`, вешаем функцию
на событие `\yii\base\Application::EVENT_BEFORE_REQUEST`

# Переводы списков

Для перевода списков используем следующую стандартизированную структуру:
Таблица исходных значений:
- id - int - идентификатор строки
- name - varchar(255) - название строки, будет использовано если не будет найдено значения на соответствующем языке
- ... - другие поля
Например она называется `languages` (<name>)

Соответственно для нее должна быть таблица с переводами: `<name>_translation`
- id - int - идентификатор строки
- parent_id - int - идентификатор переводимой строки (languages.id)
- name - varchar(255) - название строки
- language - varchar(16) - код языка который используется в переменной `Yii::$app->language`
- ... - другие поля

# Переводы картинок

Переводы картинок хранится в строковых переменных.
см. раздел [Картинки](#Картинки)

# Картинки

Используется таже технология что и для строк, при чем когда производится редактирование и управление переводами картинок то используется еще менеджер картинок.
Путь для картинок переводов: `/cabinet/web/images/translation/<Код Языка>/<Идентификатор картинки>_b.ext`.
Путь для картинок переводов (предпросмотр) 100*100: `/cabinet/web/images/translation/<Код Языка>/<Идентификатор картинки>_t.ext`.


# Хранение переводов

Для хранения используется  [yii\i18n\DbMessageSource](http://www.yiiframework.com/doc-2.0/yii-i18n-dbmessagesource.html)
при этом можно использовать отдельную базу данных. И еще сразу закешировать эти строки в кеше.

Для хранения используются две таблицы:

~~~
CREATE TABLE source_message (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    category VARCHAR(32),
    message TEXT,
    type int
);

CREATE TABLE message (
    id INTEGER,
    language VARCHAR(16),
    translation TEXT,
    PRIMARY KEY (id, language),
    CONSTRAINT fk_message_source_message FOREIGN KEY (id)
        REFERENCES source_message (id) ON DELETE CASCADE ON UPDATE RESTRICT
);
~~~

# Категории переводов

Каждая категория в Yii может храниться в разных форматах. Указывается это в файле `main.php`.
```
return [
    // ...
    'components' => [
		// ...
        // Языки
        'i18n' => [
            'translations' => [
	            'menu*' => [
		            'class' => 'yii\i18n\PhpMessageSource',
		            'basePath' => '@app/messages',
		            'sourceLanguage' => 'en-US',
		            'fileMap' => [
			            'menu' => 'menu.php'
		            ],
	            ],
                // ...
            ],
        ],
    ],
    // ...
];
```

Для нашей системы задана конфигурация такая:
```
return [
    // ...
    'components' => [
		// ...
        // Языки
        'i18n' => [
            'translations' => [
	            'menu*' => [
		            'class' => 'yii\i18n\PhpMessageSource',
		            'basePath' => '@app/messages',
		            'sourceLanguage' => 'en-US',
		            'fileMap' => [
			            'menu' => 'menu.php'
		            ],
	            ],
                // ...
            ],
        ],
    ],
    // ...
];
```

Она задается в `/common/config/main.php`.

```
return [
    // ...
    'components' => [
		// ...
        // Языки
        'i18n'           => [
            'translations' => [
                '*'    => [
                    'class'            => '\common\components\DbMessageSourceSky',
                    'sourceLanguage'   => 'ru',
                    'enableCaching'    => true,
                    'forceTranslation' => true,
                    'cache'            => [
                        'class'     => 'yii\caching\FileCache',
                        'cachePath' => '@cabinet/runtime/languages',
                    ],
                ],
                'app*' => [
                    'class'            => '\common\components\DbMessageSourceSky',
                    'sourceLanguage'   => 'ru',
                    'enableCaching'    => true,
                    'forceTranslation' => true,
                    'cache'            => [
                        'class'     => 'yii\caching\FileCache',
                        'cachePath' => '@cabinet/runtime/languages',
                    ],
                ],
            ],
        ],
    ],
    // ...
];
```

Категория `app*` указана по мимо `*` которая явно перекрывает `app*`, потому что в движке есть проверка на услови что если нет такой категории `app` то она буде создана с настройками по умолчанию, то есть с классом `yii\i18n\PhpMessageSource`.

Как видно из настроек, в языках используется файловый кеш, который лежит в папке `@cabinet/runtime/languages`.
Настройка `sourceLanguage` не актуальна, потому что не активирован `forceTranslation`, которая говорит о том что не нужно использовать ключ языковой переменной для ее перевода.


## Способы обозначения категории в индексе

`menu*` - Это значит что применяется для категори `main` и всех потомков таких как `main/page1`, `main/widgets/checkbox` и др.
`menu` - Это значит что применяется только для категори `main`.


# Роли в админке

`languages` - роль переводчика.
Может редактировать переводы только того языка который был определен настройками выставленными `Руководителем переводчиков`.

`languages-admin` - роль руководителя переводчиков.
Может определять список переводчиков и языки которые им доступны.

# Редактирование переводов

![img_20160413_111803203](images/languages-index.png)

Поля отмеченные красным означают что поле не заплнено (не имеет перевода).

Чтобы добавить строку нужно нажать `Добавить`.

# Процедура размещения языковых переменных с DEV на PROD

Для того чтобы разработческие строки (DEV) перенести на тестирование (TEST) или бой (PROD) нужно экспортировать их и 
это файл положить в папку `/console/migrations`.
Этот скрипт добавляет новые строки и обновляет существующие.

*Импорт*

Для импорта выгруженного файла можно пользоваться админкой.
- Зайдите в админку в раздел "Язык"
- Нажмите кнопку "Обзор"
- Нажмите кнопку "Импорт"

![images](images/languages-index2.png)

# Что дополнительно сейчас существует на переводах

## Варианты переводов

1. Перевод хранится в вертикальной ячейке 

![languages-index_4.png](images/languages-index_4.png)

2. Перевод хранится в горизонтальной ячейке

![languages-index_3.png](images/languages-index_3.png)

## Что переводится

1. Таблица `news_text` поле `text_short` и `text_long`, фильтрующее поле `language` и `news_id` (технология организации переводов: перевод хранится в вертикальной ячейке)
2. Таблица `packet_description` поле `description`, фильтрующее поле `language` и `packet_id` (технология организации переводов: перевод хранится в вертикальной ячейке)
3. Таблица `banners` поле `href` и `image_src` (технология организации переводов: перевод хранится в горизонтальной ячейке), тип формирования имени поля: 2
4. Таблица `countries` поле `title` (технология организации переводов: перевод хранится в горизонтальной ячейке), тип формирования имени поля: 1
5. Таблица `regions` поле `title` (технология организации переводов: перевод хранится в горизонтальной ячейке), тип формирования имени поля: 1
6. Таблица `cities` поле `title`, `area`, `region` (технология организации переводов: перевод хранится в горизонтальной ячейке), тип формирования имени поля: 1
7. Таблица `city_type` поле `city_type_name` (технология организации переводов: перевод хранится в горизонтальной ячейке), тип формирования имени поля: 1
8. Таблица `street_type` поле `street_type` (технология организации переводов: перевод хранится в горизонтальной ячейке), тип формирования имени поля: 1
8. Таблица `usertype` поле `type` (технология организации переводов: перевод хранится в горизонтальной ячейке), тип формирования имени поля: 2

Есть два способа формирования имени поля в таблице для "горизонтального хранения" перевода
1. для поля русского языка используется суфикс `_ru` для остальных языков также суфикс своего языка
2. для поля русского языка не используется суфикс, для остальных языков используется суфикс своего языка через подчерк, например `_en`


## Просмотр переводимых языков

Если у пользователя есть роль `languages`, то он имеет возможность просматривать их на сайте. 
Задать статус языка можно в разделе "Языки.Админ / Список" задается в таблице `languages` поле `status`.
1 - режим редактирования 
2 - режим рабочий 

## Смена языка на сайте

Когда пользователь включает другой язык то скрипт ставит параметр COOKIE `language` идентификатор языка.
Скрипт лежит в `/vendor/pjhl/yii2-multilanguage/assets/static/ChangeLanguage.js`.

Установка в коде происходит в событии `Application::EVENT_VEFORE_REQUEST` через вызов `Yii::$app->languageManager->eventApplicationBeforeRequest();`.
Это событие указывается в файле `/cabinet/config/main.php`.

## Просмотр языков на "живом" сайте

Если в коде вывод строки прописан через функцию.
`Sky::t()`, то на "живом" сайте эта строка будет подсвечиваться и ее можно отредактировать.
Такие переменные оборачивается в div с классом `sky-edit-string`.
Через функцию `Sky::ti()` выводится картинка
Такая картинка содержит класс `sky-edit-image`.

Вызов 

`\common\base\Sky::t('app', 'Заголовок 1');`
Результат
`<div class="sky-edit-string">Заголовок 1</div>`

`\common\base\Sky::ti('app', 'Баннер на главной', ['width' => 100, 'class' => 'thumbnail']);`
Результат
`<img class="thumbnail sky-edit-image" width="100" src="/images/1.png">`

# Перевод картинок

![languages](images/languages-index_2.png)

1 - ведет на `/languages/category`
где `POST`
`id` - идентификатор строки
`language` - двухбуквенный код языка
Через `POST` передается картинка под именем поля `file`

# Сервис с языками

Можно сделать нормализацию колонок по всем таблицам с "горизонтальным хранением" строк
Запускается:
```
php yii languages/normalize-tables
```

*Добавлеие*

Добавить язык новый можно через консольную команду:
```
php yii languages/add code=zz
```
где `code` *Код* языка

При этом создается язык со статусом `1`. На редактировании.
И добавляются колонки в таблицы там, где это необходимо.

*Удаление*

Удалить язык можно через команду
```
php yii languages/add code=zz
```
где `code` *Код* языка

При этом удаляется язык, удаляются колонки в таблицах там, где это необходимо, (строки для перевода и 
картинки если они есть)(в разработке).


# Событие для потерянных переводов

Если возникает ситуация что для отпределнной строки нет перевода то генерируется(триггер) событие `missingTranslation`,
 которое у нас обрабатывается функцией.
`\common\components\DbMessageSourceSky::handleMissingTranslation()`

Задается она в главном конфигурационном файле `main.php`

```
return [
    'components'  => [
        'i18n'         => [
            'translations' => [
                'app*'     => [
                    // ...
                    'on missingTranslation' => ['\common\components\DbMessageSourceSky', 'handleMissingTranslation'],
                ],
            ],
        ],
    ],
];
```

Есть опция которая отвечает за выбор стратегии подстановки недостающего перевода.
Эта опция хранится в таблице `configs` под именем `translation.missingEvent.strategy`.
1 - используется строка ключ
2 - используется русский перевод, если его нет то пишется "ОШИБКА"
Эта переменная выбирается в адмике на странице ""

# Переводы для пакетов

Пакеты характеризуются двумя характеристиками:
1. Обложка-картинка
2. Описание пакета

Предлагаю сделать так
Категория для переводов "Пакеты"
ключ для картинки = 'image_<id>'
ключ для описания = 'description_<id>'

# Для выкладки (test)

git pull
php yii migrate
php yii languages/insert-categories
php yii languages/delete-category c.L3z5FhN_tl826sxKGxhm_tRPq2idlC
php yii languages/delete-category c.Qlla4p9_OGYo9SBI2R5OI1SBQ8-4Hs
php yii languages/convert-images
php yii languages/convert-descriptions
php yii languages/convert-user-status стирает старую категорию и добавляет новую

c.ih1JEiyTNDkjYGGD_-nBCZquDGx5oC - статусы

Выкладка на TEST7

1. Накатить таблицы message_prod, source_message_prod, languages_category_tree

2. git pull

3. php yii languages/update-prod = заливает две таблицы message и source_message PROD <- TEST7 по стратегии обновления
переименовываю таблицы

4. Переименование таблиц

message => message1
message_prod => message
message1 => message_prod

source_message => source_message1
source_message_prod => source_message
source_message1 => source_message_prod

languages_category_tree => languages_category_tree1
languages_category_tree_prod => languages_category_tree
languages_category_tree1 => languages_category_tree_prod

5. php yii languages/convert-images

6. php yii languages/convert-descriptions

7. php yii languages/convert-user-status

8. php yii languages/normalize-tables (PROD)

9. Удалить картинки пакетов и описания пакетов

Выкладка на STAGE

TEST9->STAGE:
languages_access
languages_category_tree
message
source_message

TEST9 copy /cabinet/web/images/translation TEST9->STAGE

TEST9 git pull

TEST9 php yii migrate
TEST9 php yii languages/normalize-tables


# Переводы полей (в разработке)

Всего используется два вида полей для переводов:
- Строки
- Картинки

Для этого можно расширить Класс `ActiveRecord` чтобы обращаясь к переменной возвращалось переведенное значение. Например:

```php
// в переменную $comment будет помещен коментарий уже переведенный на язык который используется в системе
$comment = $transaсtion->coment;
```

Пример класса от которого нужно наследоваться чтобы поля были переводимыми:
```php
class Transaction extends TranslatedActiveRecord
{
    return translationFields()
    {
        return ['coment'];
    }

    // ...
}
```

Промежуточный класс:

```php
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class TranslatedActiveRecord extends ActiveRecord
{
    /**
     * @return array
     * Два варианта возврата:
     *
     * Такие поля используются для перевода как строки:
     * [
     *     '<Название поля для перевода>'
     * ]
     *
     * Такие поля используются для перевода как строки и картини:
     * [
     *     'text' => [
     *          '<Название поля для перевода>',
     *          ...
     *          ],
     *     'image' => [
     *          '<Название поля для перевода>',
     *          ...
     *          ],
     * ]
     *
     */
    public function translationFields()
    {
        return [];
    }

    /**
     * Возвращает ответ на вопрос: Поле $name является полем для перевода?
     *
     * @return bool
     * true - поле является полем для перевода
     * false - поле не является полем для перевода
     */
    private function isTranslationField($name)
    {
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes and related objects can be accessed like properties.
     *
     * @param string $name property name
     * @throws \yii\base\InvalidParamException if relation name is wrong
     * @return mixed property value
     * @see getAttribute()
     */
    public function __get($name)
    {
    }

    /**
     * Возвращает поле в языке который установлен в приложении
     * Если оно Текущий язык в системе является языком в который принят по умолчанияю то возвращается значение поля $name
     * иначе оно выбирается из поля `<$name>_translation`
     *
     * @param $name
     * @return mixed
     */
    public function __getTranslated($name)
    {
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes and related objects can be accessed like properties.
     *
     * @param string $name property name
     * @throws \yii\base\InvalidParamException if relation name is wrong
     * @return mixed property value
     * @see getAttribute()
     */
    public function __set($name, $value)
    {
    }

    public function __setTranslated($name, $value)
    {
    }
}
```

Чтобы задать в поле сразу несколько вариантов перевода, при добавлении записи то это можно сделать так
```php
$t = new Translation();
$t->comment = [
    'ru' => 'Перевод',
    'en' => 'Transaction',
];
$t->save();
```

## Строки

Применяется когда необходимо перевести поле таблицы, например поле `transaction.coment`.

Для этого поле создается доп поле `<name>_translation`, где `<name>` - имя исходного поля. Тип данных TEXT.
Формат хранения данных: JSON
~~~
[
    [
        <Код Языка>, '<текст>'
    ],
    ...
]
~~~

Если в приложении используется язык по умолчанию `\common\components\LanguageService::isDefault()` то берется поле `<name>` иначе испольуется поле `<name>_translation`

# Картинки (в разработке)

Используется таже технология что и для строк, при чем когда производится редактирование и управление переводами картинок то используется еще менеджер картинок.
Путь для картинок переводов: `/cabinet/web/images/translation/<Код Языка>/<Идентификатор картинки>_b.ext`.
Путь для картинок переводов (предпросмотр) 100*100: `/cabinet/web/images/translation/<Код Языка>/<Идентификатор картинки>_t.ext`.

Пример: Есть таблица заказа и в ней поле `image` которое переводится. Это значит что в базе будет сущкствовать поле `image` и `image_translation`
Класс заказа должен быть создан от родительского `\common\base\TranslatedActiveRecord` и укзать в нем
```php
class Order extend \common\base\TranslatedActiveRecord
{
    public function translationFields()
    {
        return ['image'];
    }
}
```

и чтобы получить в представлении (шаблоне) переведенную картинку, то нужно сделать это так:
~~~
<img src="<?= $order->image ?>" width="100">
~~~
Если надо выдать ее с полным путем, то так
~~~
<img src="<?= Url::to($order->image, true) ?>" width="100">
~~~